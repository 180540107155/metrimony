// Generated code from Butter Knife. Do not modify!
package com.example.matrimonyapp;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivitySearchUser_ViewBinding implements Unbinder {
  private ActivitySearchUser target;

  @UiThread
  public ActivitySearchUser_ViewBinding(ActivitySearchUser target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivitySearchUser_ViewBinding(ActivitySearchUser target, View source) {
    this.target = target;

    target.etActSearch = Utils.findRequiredViewAsType(source, R.id.etActSearch, "field 'etActSearch'", EditText.class);
    target.rcvUsers = Utils.findRequiredViewAsType(source, R.id.rcvUsers, "field 'rcvUsers'", RecyclerView.class);
    target.tvNoSearchDataFound = Utils.findRequiredViewAsType(source, R.id.tvNoSearchDataFound, "field 'tvNoSearchDataFound'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivitySearchUser target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etActSearch = null;
    target.rcvUsers = null;
    target.tvNoSearchDataFound = null;
  }
}
