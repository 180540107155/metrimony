// Generated code from Butter Knife. Do not modify!
package com.example.matrimonyapp.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimonyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LanguageAdapter$ViewHolder_ViewBinding implements Unbinder {
  private LanguageAdapter.ViewHolder target;

  @UiThread
  public LanguageAdapter$ViewHolder_ViewBinding(LanguageAdapter.ViewHolder target, View source) {
    this.target = target;

    target.tvSpinnerList = Utils.findRequiredViewAsType(source, R.id.tvSpinnerList, "field 'tvSpinnerList'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LanguageAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvSpinnerList = null;
  }
}
