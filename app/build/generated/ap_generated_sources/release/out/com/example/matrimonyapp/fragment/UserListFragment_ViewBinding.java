// Generated code from Butter Knife. Do not modify!
package com.example.matrimonyapp.fragment;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.matrimonyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UserListFragment_ViewBinding implements Unbinder {
  private UserListFragment target;

  private View view7f080058;

  @UiThread
  public UserListFragment_ViewBinding(final UserListFragment target, View source) {
    this.target = target;

    View view;
    target.rcvUserList = Utils.findRequiredViewAsType(source, R.id.rcvUserList, "field 'rcvUserList'", RecyclerView.class);
    target.tvNoDataFound = Utils.findRequiredViewAsType(source, R.id.tvNoDataFound, "field 'tvNoDataFound'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnAddUser, "method 'onViewClicked'");
    view7f080058 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    UserListFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rcvUserList = null;
    target.tvNoDataFound = null;

    view7f080058.setOnClickListener(null);
    view7f080058 = null;
  }
}
