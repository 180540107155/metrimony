// Generated code from Butter Knife. Do not modify!
package com.example.matrimonyapp.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimonyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UserListAdapter$UserHolder_ViewBinding implements Unbinder {
  private UserListAdapter.UserHolder target;

  @UiThread
  public UserListAdapter$UserHolder_ViewBinding(UserListAdapter.UserHolder target, View source) {
    this.target = target;

    target.tvFullname = Utils.findRequiredViewAsType(source, R.id.tvFullname, "field 'tvFullname'", TextView.class);
    target.tvlanguage = Utils.findRequiredViewAsType(source, R.id.tvlanguage, "field 'tvlanguage'", TextView.class);
    target.tvAge = Utils.findRequiredViewAsType(source, R.id.tvAge, "field 'tvAge'", TextView.class);
    target.ivDeleteUser = Utils.findRequiredViewAsType(source, R.id.ivDeleteUser, "field 'ivDeleteUser'", ImageView.class);
    target.ivFavouriteUser = Utils.findRequiredViewAsType(source, R.id.ivFavouriteUser, "field 'ivFavouriteUser'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UserListAdapter.UserHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvFullname = null;
    target.tvlanguage = null;
    target.tvAge = null;
    target.ivDeleteUser = null;
    target.ivFavouriteUser = null;
  }
}
