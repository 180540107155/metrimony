// Generated code from Butter Knife. Do not modify!
package com.example.matrimonyapp.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimonyapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CityAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CityAdapter.ViewHolder target;

  @UiThread
  public CityAdapter$ViewHolder_ViewBinding(CityAdapter.ViewHolder target, View source) {
    this.target = target;

    target.tvSpinnerList = Utils.findRequiredViewAsType(source, R.id.tvSpinnerList, "field 'tvSpinnerList'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CityAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvSpinnerList = null;
  }
}
