package com.example.matrimonyapp.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimonyapp.R;
import com.example.matrimonyapp.UserListRegistration;
import com.example.matrimonyapp.adapter.UserListAdapter;
import com.example.matrimonyapp.database.TblUser;
import com.example.matrimonyapp.model.UserModel;
import com.example.matrimonyapp.util.Constant;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserListFragment extends Fragment {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userlist = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;




    public static UserListFragment  getInstance(int gender) {
        UserListFragment fragment = new UserListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.GENDER, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_userlist, null);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;
    }


    void openAddUserScreen() {
        Intent intent = new Intent(getActivity(), UserListRegistration.class);
        startActivity(intent);
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure Want To Delete This User?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(getActivity()).deleteUserById(userlist.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userlist.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userlist.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userlist.addAll(new TblUser(getActivity()).getUserlistByGender(getArguments().getInt(Constant.GENDER)));
        adapter = new UserListAdapter(getActivity(), userlist, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void OnItemClicked(int position) {
                Intent intent = new Intent(getActivity(),UserListRegistration.class);
                intent.putExtra(Constant.USER_OBJECT,userlist.get(position));
                startActivity(intent);
            }

            @Override
            public void OnFavouriteClick(int position) {
                int lastUpdatedFavouriteStatus = new TblUser(getActivity()).updateFavouriteStatus(userlist.get(position).getIsFavourite() == 0 ? 1 : 0,userlist.get(position).getUserId());
                    if(lastUpdatedFavouriteStatus > 0 ){
                        userlist.get(position).setIsFavourite(userlist.get(position).getIsFavourite() == 0 ? 1 : 0);
                        adapter.notifyItemChanged(position);
                    }
            }

        });
        rcvUserList.setAdapter(adapter);
        checkAndVisibleView();


    }

    void checkAndVisibleView() {
        if (userlist.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.btnAddUser)
    public void onViewClicked() {
        openAddUserScreen();
    }
}
