package com.example.matrimonyapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimonyapp.R;
import com.example.matrimonyapp.model.UserModel;
import com.example.matrimonyapp.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userlist;
    OnViewClickListener onViewClickListener;

    public UserListAdapter(Context context, ArrayList<UserModel> userlist,OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userlist = userlist;
        this.onViewClickListener = onViewClickListener;
    }

   public interface OnViewClickListener{
        void OnDeleteClick(int position);
        void OnItemClicked(int position);
        void OnFavouriteClick(int position);
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {

        holder.tvFullname.setText(userlist.get(position).getFirstName() + " " + userlist.get(position).getLastName());
        holder.tvlanguage.setText(userlist.get(position).getLanguage() + " | " + userlist.get(position).getCity());
        String dateParts[] = userlist.get(position).getDob().split("/");
        String age = Utils.getAge(Integer.parseInt(dateParts[2]),Integer.parseInt(dateParts[1]),Integer.parseInt(dateParts[0]));
        Log.d("::1::", "onBindViewHolder: " + dateParts[2]+ dateParts[1]+ dateParts[0] );
        holder.tvAge.setText("Age : "+age);
        holder.ivFavouriteUser.setImageResource(userlist.get(position).getIsFavourite() == 0 ? R.drawable.baseline_favorite_border_black_36 : R.drawable.baseline_favorite_black_36);

        holder.ivDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onViewClickListener != null){
                    onViewClickListener.OnDeleteClick(position);
                }

            }
        });

        holder.ivFavouriteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onViewClickListener != null){
                    onViewClickListener.OnFavouriteClick(position);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onViewClickListener != null){
                    onViewClickListener.OnItemClicked(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return userlist.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullname)
        TextView tvFullname;
        @BindView(R.id.tvlanguage)
        TextView tvlanguage;
        @BindView(R.id.tvAge)
        TextView tvAge;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavouriteUser)
        ImageView ivFavouriteUser;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
