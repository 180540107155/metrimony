package com.example.matrimonyapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimonyapp.model.CityModel;
import com.example.matrimonyapp.model.LanguageModel;

import java.util.ArrayList;

public class TblMstCity extends MyDatabase {

    public static final String TABLE_NAME = "TblMstCity";
    public  static  final  String CITY_ID = "CityId";
    public  static  final  String CITY_NAME = "CityName";

    public TblMstCity(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCitylist(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel> list = new ArrayList<>();
        String query = "SELECT * FROM "+ TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        CityModel cityModel1 = new CityModel();
        cityModel1.setCityName("Select One");
        list.add(0,cityModel1);
        for (int i = 0 ; i<cursor.getCount();i++){
            CityModel cityModel = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public  CityModel getCityId(int id){
        SQLiteDatabase db = getReadableDatabase();
        CityModel model = new CityModel();
        String query = " SELECT * FROM " + TABLE_NAME + " WHERE " + CITY_ID + " = ? ";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        cursor.close();
        db.close();
        return  model;
    }
}
