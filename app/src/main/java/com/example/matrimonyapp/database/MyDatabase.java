package com.example.matrimonyapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    public static  final String DATABASE_NAME = "Metrimony.db";
    public  static  final  int VERSION = 3;


    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }
}
