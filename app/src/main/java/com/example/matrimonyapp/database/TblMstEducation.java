package com.example.matrimonyapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimonyapp.model.CityModel;
import com.example.matrimonyapp.model.EducationModel;

import java.util.ArrayList;

public class TblMstEducation extends MyDatabase {

    public  static  final  String TABLE_NAME = "TblMstEducation";
    public  static  final  String EDUCATION_ID = "EducationId";
    public  static  final  String EDUCATION_NAME = "EducationName";

    public TblMstEducation(Context context) {
        super(context);
    }

    public ArrayList<EducationModel> getEducationlist(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<EducationModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        EducationModel educationModel1 = new EducationModel();
        educationModel1.setEducationName("Select One");
        list.add(0,educationModel1);

        for (int i =0 ; i<cursor.getCount(); i++){
            EducationModel educationModel = new EducationModel();
            educationModel.setEducationId(cursor.getInt(cursor.getColumnIndex(EDUCATION_ID)));
            educationModel.setEducationName(cursor.getString(cursor.getColumnIndex(EDUCATION_NAME)));
            list.add(educationModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return  list;
    }

}
